package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"text/template"
)

type ArtifactoryToken struct {
	Username string
	Token    string
}

func (t ArtifactoryToken) TokenBase64() string {
	return base64.StdEncoding.EncodeToString([]byte(t.Token))
}

const (
	inputDir = "/templates"
)

var (
	files = map[string]string{
		"settings.xml":      "~/.m2",
		"init.gradle":       "~/.gradle",
		"gradle.properties": "~/.gradle",
		".npmrc":            "~",
		"pip.conf":          "~/.config/pip",
	}
)

func main() {
	subDir := os.Getenv("OUTPUT_SUB_DIR")
	outputDir := os.Getenv("BITBUCKET_CLONE_DIR") + "/" + subDir
	url := os.Getenv("TOKEN_SERVICE_URL")

	if sox, err := strconv.ParseBool(os.Getenv("SOX")); err != nil {
		log.Fatalln("failed to read SOX variable. expected bool:", err)
	} else if sox {
		url = url + "?sox=true"
	}

	// Create a Bearer string by appending string access Token
	var bearer = "Bearer " + os.Getenv("PIPELINES_JWT_TOKEN")

	// Create a new request using http
	req, err := http.NewRequest(http.MethodPost, url, nil)
	if err != nil {
		log.Fatalln("Failed to create request:", err)
	}

	// add authorization header to the req
	req.Header.Add("Authorization", bearer)

	log.Println("Fetching Token from", url)
	// Send req using http Client
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln("Client failed making request:", err)
	} else if !(resp.StatusCode >= 200 && resp.StatusCode <= 299) {
		log.Fatalln("Token request failed:", resp.Status)
	}

	body, _ := ioutil.ReadAll(resp.Body)

	token := ArtifactoryToken{}
	if err = json.Unmarshal(body, &token); err != nil {
		log.Fatalln("failed to read response from token service:", err)
	}

	log.Println("Creating configuration files in", outputDir)
	_ = os.MkdirAll(outputDir, 0777)

	activateScript, err := os.OpenFile(outputDir+"/activate.sh", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0777)
	if err != nil {
		log.Fatalln("failed to create activate script: ", err)
	}

	fmt.Fprintln(activateScript, "#!/usr/bin/env bash")
	fmt.Fprintf(activateScript, "export ARTIFACTORY_USERNAME=%s\n", token.Username)
	fmt.Fprintf(activateScript, "export ARTIFACTORY_PASSWORD=%s\n", token.Token)

	for filename, dest := range files {
		t, err := template.ParseFiles(inputDir + "/" + filename)
		if err != nil {
			log.Fatalln("failed to parse templates: ", err)
		}

		f, err := os.OpenFile(outputDir+"/"+filename, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0777)
		if err != nil {
			f.Close()
			panic(err)
		}

		err = t.Execute(f, token)
		if err != nil {
			f.Close()
			panic(err)
		}
		log.Println("Created:", subDir+"/"+filename)

		f.Close()
		fmt.Fprintf(activateScript, "mkdir -p %s && cp %s/%s %s/%s\n", dest, subDir, filename, dest, filename)
	}

	activateScript.Close()
	log.Printf("Finished bootstrap. Run .artifactory/activate.sh to mount config files")
}
