# Artifactory Sidekick

Bootstraps credentials for package management

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  # Run the pipe to generate the .artifactory directory
  - pipe: atlassian/artifactory-sidekick:v1
  # Install the config files to the home directory
  - source .artifactory/activate.sh
```

## Prerequisites

## Examples

Advanced example:

```yaml
- step:
    name: Use Sidekick
    script:
      - pipe: atlassian/artifactory-sidekick:v1
        variable:
          - SOX: "true"
      - source .artifactory/activate.sh
      - mvn org.apache.maven.plugins:maven-dependency-plugin:3.1.1:get -Dartifact=com.atlassian.buildeng:test-upload:1.0.76:jar
```

## Developing

Verify deployments with this [pipeline](https://bitbucket.org/atlassian/packages-credentials-check/src/master/)


## Support
If you're reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


