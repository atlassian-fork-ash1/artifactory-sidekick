FROM golang:1.12-alpine as builder

# Install git + SSL ca certificates.
RUN apk update && apk add --no-cache git ca-certificates && update-ca-certificates

WORKDIR /src/

COPY go.mod /src/go.mod
COPY go.sum /src/go.sum

RUN go mod download

COPY main.go /src/main.go

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /go/bin/bootstrap

FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

COPY --from=builder /go/bin/bootstrap /go/bin/bootstrap
COPY templates /templates

ENTRYPOINT ["/go/bin/bootstrap"]